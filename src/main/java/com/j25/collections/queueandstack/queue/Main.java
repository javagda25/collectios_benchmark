package com.j25.collections.queueandstack.queue;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        // list - queue - kolejka
        // FIFO - first in first out
        // jeśli wchodzę do kolejki pierwszy, to będę pierwszy który wyjdzie.

        Naleśnik naleśnikTwarog = new Naleśnik(
                "Twarogowy Świat",
                new ArrayList<String>(Arrays.asList("Twaróg")),
                10.0);
        Naleśnik naleśnikMasloOrzechoweDzem = new Naleśnik(
                "PeanutbutterJelly",
                new ArrayList<String>(Arrays.asList("Maslo orzechowe", "Dzem truskawkowy")),
                15.0);
        Naleśnik nalesnikiSmietanowe = new Naleśnik(
                "Smietanowy swiat",
                new ArrayList<String>(Arrays.asList("Smietana")),
                11.0);
        Naleśnik nalesnikiBananowoCzekoladowe = new Naleśnik(
                "BananCzekoladowy",
                new ArrayList<String>(Arrays.asList("Banan", "Czekolada")),
                16.0);

        PriorityQueue<Naleśnik> integerQueue = new PriorityQueue<>(Comparator.comparingDouble(nalesnior -> nalesnior.getSkladniki().size()));
        integerQueue.add(nalesnikiSmietanowe);
        integerQueue.add(nalesnikiBananowoCzekoladowe);
        integerQueue.add(naleśnikMasloOrzechoweDzem);
        integerQueue.add(naleśnikTwarog);

        while (!integerQueue.isEmpty()){
            System.out.println(integerQueue.poll());
        }
        // pop
        // put
        // push
        // offer

//        metoda(integerQueue);
    }

//    private static void metoda(Queue<Integer> integerQueue) {
//        integerQueue.
//    }

}
