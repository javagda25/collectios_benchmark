package com.j25.collections.queueandstack.queue.poczekalnia;

public class QueueIsEmptyException extends RuntimeException{
    public QueueIsEmptyException(String man) {
        super(man);
    }
}
