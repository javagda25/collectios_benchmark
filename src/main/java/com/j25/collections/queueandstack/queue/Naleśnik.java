package com.j25.collections.queueandstack.queue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Naleśnik {
    private String name;
    private List<String> skladniki;

    private double cena;
}
