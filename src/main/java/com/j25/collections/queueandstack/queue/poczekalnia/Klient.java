package com.j25.collections.queueandstack.queue.poczekalnia;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Klient {
    private String imie;
    private boolean priorytet;
    private LocalDateTime czasDodania;

    public Klient(String imie, boolean priorytet) {
        this.imie = imie;
        this.priorytet = priorytet;
        this.czasDodania = LocalDateTime.now();
    }
}
