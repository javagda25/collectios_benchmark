package com.j25.collections.queueandstack.queue.apteka;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Customer {
    private int wiek;
    private boolean brzuchata;
    private String imie;
}
