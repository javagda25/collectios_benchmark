package com.j25.collections.queueandstack.queue.poczekalnia;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class Poczekalnia {
    private Queue<Klient> klientQueue = new PriorityQueue<>(new Comparator<Klient>() {
        @Override
        public int compare(Klient k1, Klient k2) {
            if (k1.isPriorytet() && !k2.isPriorytet()) {
                return -1;
            }
            if (k2.isPriorytet() && !k1.isPriorytet()) {
                return 1;
            }
            if (k1.getCzasDodania().isAfter(k2.getCzasDodania())) {
                return 1;
            }
            if (k2.getCzasDodania().isAfter(k1.getCzasDodania())) {
                return -1;
            }
            return 0;
        }
    });

    public Klient pobierzKolejnegoKlienta() {
        Klient k = klientQueue.poll();
        if (k == null) {
            throw new QueueIsEmptyException(":(");
        }
        return k;
    }

    public void wypisz() {
        PriorityQueue<Klient> tmpQ = new PriorityQueue<>(klientQueue);

//        System.out.println();
//        Iterator<Klient> iterator = tmpQ.iterator();
//        while (iterator.hasNext()){
//            System.out.println(iterator.next());
//        }
//        System.out.println();
//        for (Klient klientk: tmpQ) {
//            System.err.println(klientk);
//        }
        System.out.println();
        while (!tmpQ.isEmpty()) {
            System.out.println(tmpQ.poll());
        }
    }

    public void dodaj(Klient klient) {
        klientQueue.offer(klient);
    }
}
