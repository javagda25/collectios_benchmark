package com.j25.collections.queueandstack.stack;

import java.util.Stack;

public class Main {
    public static void main(String[] args) {
        // LIFO - last in first out
        Stack<Integer> lifo = new Stack<>();

        lifo.push(1);
        lifo.push(2);
        lifo.push(3); // ostatni który został wstawiony, będzie pierwszym którego wyciągniemy

        for (Integer element : lifo) {
            System.out.println(element);
        }

        lifo.add(2, 5); // dodanie elementu na index 2

        System.out.println();
        while (!lifo.empty()) {
            // dopóki nie jest pusty (stos)
            // wypisz następny i ściągnij go ze stosu
            System.out.println(lifo.pop());
        }


    }
}
