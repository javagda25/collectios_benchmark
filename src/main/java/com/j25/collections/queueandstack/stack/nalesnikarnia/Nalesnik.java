package com.j25.collections.queueandstack.stack.nalesnikarnia;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class Nalesnik {
    private LocalDateTime czasStworzenia;
    private List<String> skladniki;

    public Nalesnik(List<String> skladniki) {
        this.skladniki = skladniki;
        this.czasStworzenia = LocalDateTime.now();
    }
}
