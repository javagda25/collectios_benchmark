package com.j25.collections.benchmarks;

import com.j25.collections.linkedlist.Wezel;

public class MyLinkedList<T> {
    private Wezel<T> head; // 0
    private Wezel<T> tail; // ostatni

    private int size = 0;

    public MyLinkedList() {
        head = null;
        tail = null;
    }

    public void add(T element) {
        Wezel<T> wezel = new Wezel<>(element);
        if (size == 0) {
            head = wezel;
            tail = wezel;
        } else if (size > 0) {
            tail.setNastepnik(wezel);
            wezel.setPoprzednik(tail);

            tail = wezel;
        }

        size++;
    }

    public void add(int index, T element) {
        if (index < 0 || index > size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if (index == size) {
            add(element);
            return;
        }
        int licznik = 0;

        Wezel<T> tymczasowy = head;
        while (licznik++ < (index)) {
            tymczasowy = tymczasowy.getNastepnik();
        }

        Wezel<T> poprzednik = tymczasowy.getPoprzednik();
        Wezel<T> nastepnik = tymczasowy;

        Wezel<T> nowyWezel = new Wezel<>(element);
        nowyWezel.setPoprzednik(poprzednik);
        nowyWezel.setNastepnik(nastepnik);

        if (poprzednik != null) { // przypadek kiedy edytujemy w srodku
            poprzednik.setNastepnik(nowyWezel);
        }
        if (tymczasowy == head) {
            head = nowyWezel;
        }
        nastepnik.setPoprzednik(nowyWezel);

        size++;
    }

    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        Wezel<T> tymczasowy = head;
        int licznik = 0;
        while (licznik++ < index) {
            tymczasowy = tymczasowy.getNastepnik();
        }
        return tymczasowy.getData();
    }

    public void remove(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        Wezel<T> tymczasowy = head;
        int licznik = 0;
        while (licznik++ < index) {
            tymczasowy = tymczasowy.getNastepnik();
        }

        Wezel<T> poprzednik = tymczasowy.getPoprzednik();
        Wezel<T> nastepnik = tymczasowy.getNastepnik();

//        tymczasowy.getPoprzednik().setNastepnik(tymczasowy.getNastepnik());
        if (poprzednik != null)
            poprzednik.setNastepnik(nastepnik);
        if (nastepnik != null)
            nastepnik.setPoprzednik(poprzednik);

        if (tymczasowy == head) { // usuwamy 1 element
            head = nastepnik;
        }

        size--;
    }

    public int size() {
        return size;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("[");
        if (size > 0) {

            Wezel<T> tymczasowy = head;
            while (tymczasowy != null) {
                builder.append(tymczasowy.getData());
                builder.append(", ");
                tymczasowy = tymczasowy.getNastepnik();
            }

            // remove last 2 signs
            builder.delete(builder.length() - 2, builder.length());
        }
        builder.append("]");
        return builder.toString();
    }
}
