package com.j25.collections.linkedlist;

public class Wezel<Y> {
    private Y data;
    private Wezel<Y> nastepnik;
    private Wezel<Y> poprzednik;

    public Wezel(Y data) {
        this.data = data;
    }

    public Y getData() {
        return data;
    }

    public void setData(Y data) {
        this.data = data;
    }

    public Wezel<Y> getNastepnik() {
        return nastepnik;
    }

    public void setNastepnik(Wezel<Y> nastepnik) {
        this.nastepnik = nastepnik;
    }

    public Wezel<Y> getPoprzednik() {
        return poprzednik;
    }

    public void setPoprzednik(Wezel<Y> poprzednik) {
        this.poprzednik = poprzednik;
    }
}
